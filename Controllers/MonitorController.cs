using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlgoMachine.Core.Api;
using AlgoMachine.Core.Interfaces;
using AlgoMachine.Core.Models;
using AlgoMachine.Service.PositionExecutor;
using Microsoft.AspNetCore.Mvc;

namespace AlgoMachine.Api.PositionExecutor.Controllers
{
    public class MonitorController:AbstractController
    {
        private ITradeService TradeService;
        public MonitorController(ITradeService tradeService)
        {
            this.TradeService = tradeService;
        }

        [HttpGet]
        [Route("FollowingSymbols")]
        [ProducesResponseType(typeof(ApiResponse<List<string>>), 200)]
        public async Task<IActionResult> FollowingSymbols()
        {
            try
            {
                var followingSymbols = BinanceContainer.FollowingSymbols;
                return this.ApiResponse<List<string>>(followingSymbols);
            }
            catch (Exception ex)
            {
                return this.ApiErrorResponse(ex.Message);
            }
        }

        [HttpGet]
        [Route("Positions")]
        [ProducesResponseType(typeof(ApiResponse<List<Position>>), 200)]
        public async Task<IActionResult> Positions()
        {
            try
            {
                var positions = BinanceContainer.Positions;
                return this.ApiResponse<List<Position>>(positions);
            }
            catch (Exception ex)
            {
                return this.ApiErrorResponse(ex.Message);
            }
        }
        [HttpGet]
        [Route("SymbolPrices")]
        [ProducesResponseType(typeof(ApiResponse<List<CurrentPrice>>), 200)]
        public async Task<IActionResult> SymbolPrices()
        {
            try
            {
                var symbolPrices = BinanceContainer.SymbolPrices;
                return this.ApiResponse<List<CurrentPrice>>(symbolPrices);
            }
            catch (Exception ex)
            {
                return this.ApiErrorResponse(ex.Message);
            }
        }
        [HttpGet]
        [Route("UserBinanceListeners")]
        [ProducesResponseType(typeof(ApiResponse<UserBinanceListener>), 200)]
        [ProducesResponseType(typeof(ApiResponse<List<string>>), 200)]
        public async Task<IActionResult> UserBinanceListeners(string userId = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    UserBinanceListener userBinanceListener;
                    if(BinanceContainer.UserBinanceListeners.TryGetValue(userId, out userBinanceListener))
                        return this.ApiResponse<UserBinanceListener>(userBinanceListener);
                }
                return this.ApiResponse<List<string>>(BinanceContainer.UserBinanceListeners.Keys.ToList());
            }
            catch (Exception ex)
            {
                return this.ApiErrorResponse(ex.Message);
            }
        }
    }
}