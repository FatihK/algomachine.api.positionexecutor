using System;
using System.Threading.Tasks;
using AlgoMachine.Core.Api;
using AlgoMachine.Core.Interfaces;
using AlgoMachine.Core.Models;
using AlgoMachine.Service.Managers;
using Microsoft.AspNetCore.Mvc;

namespace AlgoMachine.Api.PositionExecutor.Controllers
{
    public class PositionController : AbstractController
    {
        IPositionManager PositionManager;
        ITradeService TradeService;
        public PositionController(IPositionManager positionManager, ITradeService tradeService)
        {
            this.PositionManager = positionManager;
            this.TradeService = tradeService;
        }
        [HttpGet]
        [Route("Test")]
        public async Task<IActionResult> Test()
        {
            return Content("OK");
        }
        [HttpPost]
        [Route("CreatePosition")]
        [ProducesResponseType(typeof(ApiResponse<Position>), 200)]
        public async Task<IActionResult> CreatePosition(Position position)
        {

            try
            {
                await PositionManager.CreatePositionAsync(position);
                return this.ApiResponse<Position>(position);
            }
            catch (Exception ex)
            {
                return this.ApiErrorResponse(ex.Message);
            }
        }

        [HttpPost]
        [Route("EndPosition")]
        [ProducesResponseType(typeof(ApiResponse<Position>), 200)]
        public async Task<IActionResult> EndPosition(Position position)
        {

            try
            {
                await PositionManager.EndPositionAsync(position);
                return this.ApiResponse<Position>(position);
            }
            catch (Exception ex)
            {
                return this.ApiErrorResponse(ex.Message);
            }

        }
    }
}