using System.Collections.Generic;
using System.Threading.Tasks;
using AlgoMachine.Core.Models;
using AutoMapper;
using Binance.Net.Clients;
using Binance.Net.Interfaces.Clients;
using Binance.Net.Objects;
using Microsoft.Extensions.Options;

namespace AlgoMachine.Service.PositionExecutor
{
    public static class BinanceContainer
    {
        private static IMapper Mapper;
        public static void SetMapper(IMapper mapper){
            Mapper=mapper;
        }
        private static IBinanceClient BinanceClient;
        private static Dictionary<string, UserBinanceListener> userBinanceListeners=new Dictionary<string,UserBinanceListener>();
        public static Dictionary<string, UserBinanceListener> UserBinanceListeners
        {
            get { return userBinanceListeners; }
        }
        
        private static List<Position> positions=new List<Position>();
        public static List<Position> Positions
        {
            get { return positions; }
        }
        
        private static List<string> followingSymbols =new List<string>();
        public static List<string> FollowingSymbols
        {
            get { return followingSymbols; }
        }
        
        private static List<CurrentPrice> symbolPrices =new List<CurrentPrice>();
        public static List<CurrentPrice> SymbolPrices
        {
            get { return symbolPrices; }
        }
        public async static Task<UserBinanceListener> AddAccountAsync(Account account, List<Position> positions, List<Asset> assets){
            UserBinanceListener userBinanceListener=new UserBinanceListener(account, Mapper);
            await userBinanceListener.GetUserDatasAsync();
            userBinanceListeners.TryAdd(account.Id.ToString(), userBinanceListener);
            return userBinanceListener;
        }
    }
}