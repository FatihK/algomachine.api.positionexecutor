namespace AlgoMachine.Service.PositionExecutor
{
    public class CurrentPrice
    {
        public string Symbol { get; set; }
        public decimal Price { get; set; }
    }
}