using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Threading.Tasks;
using AlgoMachine.Core.Interfaces;
using AlgoMachine.Core.Models;
using AlgoMachine.Service.PositionExecutor;
using Binance.Net;
using Binance.Net.Interfaces.Clients;
using Binance.Net.Objects;
using CryptoExchange.Net.Authentication;

namespace AlgoMachine.Service.Managers
{
    public class PositionManager:IPositionManager
    {
        // private IBinanceClient Client;
        // public PositionManager(IBinanceClient client)
        // {
        //     this.Client=client;
        // }
        
        public async Task CreatePositionAsync(Position position){
            var orderSide=position.IsLong?Binance.Net.Enums.OrderSide.Buy:Binance.Net.Enums.OrderSide.Sell;
            var positionSide=position.IsLong?Binance.Net.Enums.PositionSide.Long:Binance.Net.Enums.PositionSide.Short;

            // var orderResult=await Client.UsdFuturesApi.Trading.PlaceOrderAsync(position.Symbol,
            // orderSide,
            // Binance.Net.Enums.FuturesOrderType.Limit,
            // position.BuyQuantity,
            // position.BuyPrice,
            // positionSide,
            // Binance.Net.Enums.TimeInForce.GoodTillCanceled,
            // null,
            // System.Guid.NewGuid().ToString());
        }
        public async Task EndPositionAsync(Position position){
            var orderSide=position.IsLong?Binance.Net.Enums.OrderSide.Buy:Binance.Net.Enums.OrderSide.Sell;
            var positionSide=position.IsLong?Binance.Net.Enums.PositionSide.Long:Binance.Net.Enums.PositionSide.Short;
            
            // var orderResult=await Client.UsdFuturesApi.Trading.PlaceOrderAsync(position.Symbol,
            // orderSide,
            // Binance.Net.Enums.FuturesOrderType.Limit,
            // position.SellQuantity,
            // position.SellPrice,
            // positionSide,
            // Binance.Net.Enums.TimeInForce.GoodTillCanceled,
            // null,
            // System.Guid.NewGuid().ToString());
        }
    }
}