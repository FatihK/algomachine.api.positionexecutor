using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AlgoMachine.Core.Interfaces;
using AlgoMachine.Core.Models;
using AutoMapper;
using Binance.Net.Clients;
using Binance.Net.Interfaces.Clients;
using Binance.Net.Objects;
using Microsoft.Extensions.Options;

namespace AlgoMachine.Service.PositionExecutor
{
    public class UserBinanceListener
    {
        private readonly string UsdFuturesApiURL = "https://testnet.binancefuture.com";
        public Account Account { get; set; }
        public string ListenKey { get; set; }
        public IBinanceClient BinanceClient { get; set; }
        public List<Position> Positions;

        private IMapper Mapper;
        public UserBinanceListener(Account account, IMapper mapper)
        {
            this.Account = account;
            this.Mapper = mapper;

            BinanceClientOptions options = new BinanceClientOptions();
            options.UsdFuturesApiOptions.BaseAddress = UsdFuturesApiURL;
            options.ApiCredentials = new CryptoExchange.Net.Authentication.ApiCredentials(Account.PublicKey, Account.SecretKey);
            this.BinanceClient = new Binance.Net.Clients.BinanceClient(options);

        }
        public async Task GetUserDatasAsync()
        {
            var positions = await this.BinanceClient.UsdFuturesApi.Trading.GetOpenOrdersAsync();
            if (positions.Success && positions.Data.Count() > 0)
            {
                Positions = Mapper.Map<List<Core.Models.Position>>(positions.Data.ToList());
            }

            var listenKeyRequest = await BinanceClient.UsdFuturesApi.Account.StartUserStreamAsync();
            if (listenKeyRequest.Success)
                ListenKey = listenKeyRequest.Data;
            else
            {
                //TODO: hatayı dbye işle
            }
        }
    }
}