using System.Globalization;
using AutoMapper;
using CM = AlgoMachine.Core.Models;
using DM = AlgoMachine.Data.Models;
namespace AlgoMachine.Service
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<DM.Account,CM.Account>().ReverseMap();
            CreateMap<CM.Position, Binance.Net.Objects.Models.Futures.BinancePositionDetailsUsdt>().ReverseMap();
        }
    }
}