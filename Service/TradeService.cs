using System.Collections.Generic;
using System.Threading.Tasks;
using AlgoMachine.Core.Interfaces;
using AlgoMachine.Service.PositionExecutor;
using AutoMapper;
using Binance.Net.Objects;
using Microsoft.Extensions.Options;
using System;
using Binance.Net.Interfaces.Clients;
using Binance.Net.Clients;

namespace AlgoMachine.Service
{
    public class TradeService : ITradeService
    {
        private IAccountManager AccountManager;
        private IMapper Mapper;
        private IBinanceSocketClient BinanceSocketClient;
        public TradeService(IAccountManager accountManager, IMapper mapper)
        {
            this.AccountManager = accountManager;
            this.Mapper = mapper;

            BinanceSocketClient = new BinanceSocketClient();
            // BinanceContainer.SetBinanceClient(BinanceClient);

        }
        public async Task ListenTickerUpdatesAsync()
        {

            await BinanceSocketClient.UsdFuturesStreams.SubscribeToAllMiniTickerUpdatesAsync(async (onMessage)=> {
                foreach(var item in onMessage.Data){
                    if(BinanceContainer.FollowingSymbols.IndexOf(item.Symbol)>-1)
                        await ChangeSymbolCurrentPrice(item.Symbol, item.LastPrice);
                }
            });
        }
        public async Task InitUserListenersAsync()
        {
            var accounts = AccountManager.GetActiveAccounts();
            BinanceContainer.SetMapper(this.Mapper);
            foreach (var account in accounts)
            {
                var userBinanceListener = await BinanceContainer.AddAccountAsync(account, null, null);
                await SubscribeAccount(userBinanceListener.ListenKey);
            }
        }
        public async Task StopListenTickerUpdatesAsync()
        {
            await BinanceSocketClient.UnsubscribeAllAsync();
            BinanceSocketClient.Dispose();
            BinanceSocketClient = null;
        }
        public static async Task ChangeSymbolCurrentPrice(string Symbol, decimal Price)
        {
            int index, count;
            index = -1;
            count = BinanceContainer.SymbolPrices.Count;
            if (count == 0)
                BinanceContainer.SymbolPrices.Add(new CurrentPrice() { Symbol = Symbol, Price = Price });

            CurrentPrice currentPrice;
            for (int i = 0; i < count; i++)
            {
                currentPrice = (CurrentPrice)BinanceContainer.SymbolPrices[i];
                if (currentPrice.Symbol == Symbol)
                    index = i;
            }
            currentPrice = new CurrentPrice();
            currentPrice.Symbol = Symbol;

            if (index > -1 && currentPrice.Price != Price)
            {
                currentPrice.Price = Price;
                BinanceContainer.SymbolPrices[index] = currentPrice;
                await SymbolPriceChanged(currentPrice);
            }
            else
                BinanceContainer.SymbolPrices.Add(currentPrice);
        }
        private static async Task SymbolPriceChanged(CurrentPrice currentPrice)
        {
            //TODO: İlgili coine ait pozisyonların fiyatları kontrol edilip ilgili order borsaya iletilecek ve DB'ye kayıt edilecek.
        }
        private async Task SubscribeAccount(string listenKey)
        {
            await BinanceSocketClient.UsdFuturesStreams.SubscribeToUserDataUpdatesAsync(listenKey,
            (leverage) =>
            {
                System.Console.WriteLine("fired:leverage");
            },
            (margin) =>
            {
                System.Console.WriteLine("fired:margin");
            },
            (accountUpdate) =>
            {
                System.Console.WriteLine("fired:accountUpdate");
                var positions = accountUpdate.Data.UpdateData.Positions;
                var balances = accountUpdate.Data.UpdateData.Balances;
            },
            (orderUpdate) =>
            {
                System.Console.WriteLine("fired:orderUpdate");
                var order = orderUpdate.Data.UpdateData;
            },
            (key) =>
            {
                System.Console.WriteLine("fired:key");

            });
        }
    }
}