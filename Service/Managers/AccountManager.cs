using System.Collections.Generic;
using AlgoMachine.Core.Interfaces;
using CM = AlgoMachine.Core.Models;
using DM = AlgoMachine.Data.Models;
using AutoMapper;
using System.Threading.Tasks;
using AlgoMachine.Data;
using System.Linq;
using System;
using AlgoMachine.Core.Models;

namespace AlgoMachine.Service.Managers
{
    public class AccountManager:IAccountManager
    {
        private IRepository DefaultRepository;
        private IMapper Mapper;
        public AccountManager(IRepository defaultRepository, IMapper mapper)
        {
            this.DefaultRepository=defaultRepository;
            this.Mapper=mapper;
        }

        public async Task<Account> CreateAccountAsync(Account account)
        {
            //TODO: kayıttan önce hesap kontrolleri yapılmalı.
            var entity = Mapper.Map<DM.Account>(account);
            await DefaultRepository.AddAsync<DM.Account>(entity);
            await DefaultRepository.SaveChangesAsync();
            return Mapper.Map<CM.Account>(entity);
        }

        public async Task<bool> DeleteAccountAsync(Account account)
        {
            var entity=await DefaultRepository.SingleAsync<Account>(a=>a.Id==account.Id);
            if(entity!=null)
            {
                //TODO: hesaba ait diğer kayıtlar da silinebilir.
                DefaultRepository.Remove<Account>(entity);
                await DefaultRepository.SaveChangesAsync();
                return await Task.FromResult(true);
            }
            return await Task.FromResult(false);
        }

        public List<CM.Account> GetActiveAccounts()
        {
            var now=DateTime.Now;
            var accounts= DefaultRepository//.AllAsync<DM.Account>();//
            .Find<DM.Account>(a=>a.ExpireDate>=now).ToList();

            List<CM.Account> accountList= Mapper.Map<List<CM.Account>>(accounts);
            return accountList;
        }

        public async Task<Account> GetSingleAccountAsync(int id)
        {
            var entity=await DefaultRepository.SingleAsync<DM.Account>(a=>a.Id==id);
            return Mapper.Map<CM.Account>(entity);
        }

        public async Task<Account> GetSingleAccountAsync(string securityCode)
        {
            var entity=await DefaultRepository.SingleAsync<DM.Account>(a=>a.SecurityCode==securityCode);
            return Mapper.Map<CM.Account>(entity);
        }

        public async Task<Account> UpdateAccountAsync(Account account)
        {
            var entity = Mapper.Map<DM.Account>(account);
            DefaultRepository.SaveItem<DM.Account>(entity);
            await DefaultRepository.SaveChangesAsync();
            return Mapper.Map<CM.Account>(entity);
        }
    }
}