using System.Threading;
using System.Threading.Tasks;
using AlgoMachine.Core.Interfaces;
using Microsoft.Extensions.Hosting;

namespace AlgoMachine.Api.PositionExecutor
{
    public class BinancePriceFetchHostedService : IHostedService
    {
        public ITradeService TradeService;
        public BinancePriceFetchHostedService(ITradeService tradeService)
        {
            this.TradeService=tradeService;
        }
        public bool IsRunning { get; set; }
        public async Task StartAsync(CancellationToken cancellationToken)
        {
            
            if(!IsRunning)
            {
                await TradeService.InitUserListenersAsync();
                await TradeService.ListenTickerUpdatesAsync();
                IsRunning = true;
            }
            await Task.CompletedTask;
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            IsRunning=false;
            await TradeService.StopListenTickerUpdatesAsync();
        }
    }
}