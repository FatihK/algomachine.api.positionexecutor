﻿using System;
using AlgoMachine.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace AlgoMachine.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }
        public DbSet<PortfolioAsset> PortfolioAssets { get; set; }
        public DbSet<Position> Positions { get; set; }
        public DbSet<PositionOrder> PositionOrders { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            if (!options.IsConfigured)
            {
                //options.UseNpgsql("User ID=postgres;Password=1326;Host=207.244.246.131;Port=5432;Database=algomachine;CommandTimeout=300;"//,b => b.MigrationsAssembly("AlgoMachine.Api")
                //);//.UseSqlite("Data Source=AlgoMachineDB.db");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
