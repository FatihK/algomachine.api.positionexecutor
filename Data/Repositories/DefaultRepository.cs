﻿using AlgoMachine.Core.Interfaces;
using AlgoMachine.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AlgoMachine.Data.Repositories
{
    public class DefaultRepository:IRepository
    {
        private readonly DataContext DataContext;
        public DefaultRepository(IServiceScopeFactory serviceProvider)
        {
            this.DataContext = serviceProvider.CreateScope().ServiceProvider.GetRequiredService<DataContext>();
        }
        public async Task<IEnumerable<TEntity>> AllAsync<TEntity>() where TEntity : class
        {
            var entities = DataContext.Set<TEntity>();
            return await entities.ToListAsync();
        }

        public IEnumerable<TEntity> Find<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return DataContext.Set<TEntity>().Where(predicate);
        }

        public async Task<TEntity> SingleAsync<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return await DataContext.Set<TEntity>().SingleOrDefaultAsync(predicate);
        }

        public async Task AddAsync<TEntity>(TEntity entity) where TEntity : class
        {
            await DataContext.Set<TEntity>().AddAsync(entity);
        }

        public async Task AddRangeAsync<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            await DataContext.Set<TEntity>().AddRangeAsync(entities);
        }

        public void Remove<TEntity>(TEntity entity) where TEntity : class
        {
            DataContext.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            DataContext.Set<TEntity>().RemoveRange(entities);
        }
        public void SaveItem<TEntity>(TEntity entity) where TEntity : class
        {
            DataContext.Entry<TEntity>(entity).State = EntityState.Modified;
        }
        public async Task SaveChangesAsync()
        {
            await DataContext.SaveChangesAsync();
        }
    }
}
