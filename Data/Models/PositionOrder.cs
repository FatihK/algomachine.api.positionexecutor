
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlgoMachine.Data.Models
{
    public class PositionOrder
    {
        [Key]
        public int Id { get; set; }
        public long AssignedOrderId { get; set; }
        public string ClientOrderId { get; set; }
        [ForeignKey("Position")]
        public int PositionId { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Symbol { get; set; }
        public OrderSide OrderSide { get; set; }
        public OrderType OrderType { get; set; }
        public PositionSide PositionSide { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public DateTime OrderDate { get; set; }
        public decimal Quantity { get; set; }
        public decimal FilledQuantity { get; set; }
        public decimal Price { get; set; }
        public decimal AveragePrice { get; set; }

        public virtual Position Position { get; set; }
    }
}