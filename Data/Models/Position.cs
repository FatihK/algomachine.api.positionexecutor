using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using AlgoMachine.Core.Models.Enums;

namespace AlgoMachine.Data.Models
{
    public class Position
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Portfolio")]
        public int PortfolioId { get; set; }
        [ForeignKey("TradeSystem")]
        public int TradeSystemId { get; set; }
        public string Symbol { get; set; }
        public PositionState PositionState { get; set; }
        public decimal BuyQuantity { get; set; }
        public decimal BuyPrice { get; set; }
        public DateTime BuyDate { get; set; }
        public decimal SellQuantity { get; set; }
        public decimal SellPrice { get; set; }
        public DateTime SellDate { get; set; }
        public bool StopLossActive { get; set; }
        public decimal StopLossPrice { get; set; }
        public bool TakeProfitActive { get; set; }
        public decimal TakeProfit1Price { get; set; }
        public decimal TakeProfit1PriceRate { get; set; }
        public decimal TakeProfit1AmountRate { get; set; }
        public decimal TakeProfit2Price { get; set; }
        public decimal TakeProfit2PriceRate { get; set; }
        public decimal TakeProfit2PriceAmountRate { get; set; }
        public decimal TakeProfit3Price { get; set; }
        public decimal TakeProfit3PriceRate { get; set; }
        public decimal TakeProfit3PriceAmountRate { get; set; }
        public decimal TakeProfit4Price { get; set; }
        public decimal TakeProfit4PriceRate { get; set; }
        public decimal TakeProfit4PriceAmountRate { get; set; }
        public decimal TakeProfit5Price { get; set; }
        public decimal TakeProfit5PriceRate { get; set; }
        public decimal TakeProfit5PriceAmountRate { get; set; }
        public bool TrailingStopLossActive { get; set; }
        public decimal TrailingStopLossRate { get; set; }
        public decimal TrailingStopLossPrice { get; set; }
        public decimal MaxDrawDownPrice { get; set; }
        public decimal MaxDrawDownRate { get; set; }
        public string PositionEnterData { get; set; }
        public string PositionExitData { get; set; }
        public decimal ProfitPercent { get; set; }
        public bool IsLong { get; set; }

        public virtual Portfolio Portfolio { get; set; }
        public virtual ICollection<PositionOrder> PositionOrders { get; set; }
    }
}