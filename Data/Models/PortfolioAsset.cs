using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlgoMachine.Data.Models
{
    public class PortfolioAsset
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Portfolio")]
        public int PortfolioId { get; set; }
        public string Symbol { get; set; }
        public MarginType MarginType { get; set; }
        public decimal Balance { get; set; }
        public int Leverage { get; set; }

        public virtual Portfolio Portfolio { get; set; }
    }
}