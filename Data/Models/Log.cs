using System;
using System.ComponentModel.DataAnnotations;

namespace AlgoMachine.Data.Models
{
    public class Log
    {
        [Key]
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string Exception { get; set; }
        public string Logger { get; set; }
    }
}