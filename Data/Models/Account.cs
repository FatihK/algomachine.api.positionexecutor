using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AlgoMachine.Data.Models
{
    public class Account
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string SecurityCode { get; set; }
        public string PublicKey { get; set; }
        public string SecretKey { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ExpireDate { get; set; }

        public List<Portfolio> Portfolios { get; set; }
    }
}