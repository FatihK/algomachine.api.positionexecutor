using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AlgoMachine.Data.Models
{
    public class Portfolio
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("Account")]
        public int AccountId { get; set; }
        public string Name { get; set; }
        
        public virtual Account Account { get; set; }
        public virtual List<PortfolioAsset> PortfolioAssets { get; set; }
    }
}