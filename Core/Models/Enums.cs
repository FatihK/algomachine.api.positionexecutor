using System;

namespace AlgoMachine.Core.Models.Enums
{
    public enum PositionState
    {
        Open=0,
        ClosedByPosition=1,
        ClosedByStrategy=2,
        ClosedByUser=3
    }
    public enum MarginType{
        Cross=0,
        Isolated=1
    }
    public enum OrderSide
    {
        Buy = 0,
        Sell = 1
    }
    //
    // Summary:
    //     Position side
    public enum PositionSide
    {
        //
        // Summary:
        //     Short
        Short = 0,
        //
        // Summary:
        //     Long
        Long = 1,
        //
        // Summary:
        //     Both for One-way mode when placing an order
        Both = 2
    }
    //
    // Summary:
    //     The status of an orderн
    public enum OrderStatus
    {
        //
        // Summary:
        //     Order is new
        New = 0,
        //
        // Summary:
        //     Order is partly filled, still has quantity left to fill
        PartiallyFilled = 1,
        //
        // Summary:
        //     The order has been filled and completed
        Filled = 2,
        //
        // Summary:
        //     The order has been canceled
        Canceled = 3,
        //
        // Summary:
        //     The order is in the process of being canceled (currently unused)
        PendingCancel = 4,
        //
        // Summary:
        //     The order has been rejected
        Rejected = 5,
        //
        // Summary:
        //     The order has expired
        Expired = 6,
        //
        // Summary:
        //     Liquidation with Insurance Fund
        Insurance = 7,
        //
        // Summary:
        //     Counterparty Liquidation
        Adl = 8
    }
    //
    // Summary:
    //     Order type for a futures order
    public enum OrderType
    {
        //
        // Summary:
        //     Limit orders will be placed at a specific price. If the price isn't available
        //     in the order book for that asset the order will be added in the order book for
        //     someone to fill.
        Limit = 0,
        //
        // Summary:
        //     Market order will be placed without a price. The order will be executed at the
        //     best price available at that time in the order book.
        Market = 1,
        //
        // Summary:
        //     Stop order. Execute a limit order when price reaches a specific Stop price
        Stop = 2,
        //
        // Summary:
        //     Stop market order. Execute a market order when price reaches a specific Stop
        //     price
        StopMarket = 3,
        //
        // Summary:
        //     Take profit order. Will execute a limit order when the price rises above a price
        //     to sell and therefor take a profit
        TakeProfit = 4,
        //
        // Summary:
        //     Take profit market order. Will execute a market order when the price rises above
        //     a price to sell and therefor take a profit
        TakeProfitMarket = 5,
        //
        // Summary:
        //     A trailing stop order will execute an order when the price drops below a certain
        //     percentage from its all time high since the order was activated
        TrailingStopMarket = 6,
        //
        // Summary:
        //     A liquidation order
        Liquidation = 7
    }
}