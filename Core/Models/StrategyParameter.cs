﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlgoMachine.Core.Models
{
    public class StrategyParameter
    {
        public int Id { get; set; }
        public int StrategyId { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public string ParameterType { get; set; }
        public string DefaultValue { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public string ValueOptions { get; set; }

        public virtual Strategy Strategy { get; set; }
    }
}
