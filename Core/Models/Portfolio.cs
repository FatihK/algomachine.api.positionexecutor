using System.Collections.Generic;

namespace AlgoMachine.Core.Models
{
    public class Portfolio
    {
        public int Id { get; set; }
        public int AccountId { get; set; }
        public string Name { get; set; }
        
        public virtual List<Asset> Assets { get; set; }
    }
}