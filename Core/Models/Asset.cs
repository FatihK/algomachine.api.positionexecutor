using AlgoMachine.Core.Models.Enums;

namespace AlgoMachine.Core.Models
{
    public class Asset
    {
        public int Id { get; set; }
        public int PortfolioId { get; set; }
        public string Symbol { get; set; }
        public MarginType MarginType { get; set; }
        public decimal Balance { get; set; }
        public int Leverage { get; set; }
    }
}