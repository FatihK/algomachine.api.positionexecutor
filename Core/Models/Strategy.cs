﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlgoMachine.Core.Models
{
    public class Strategy
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Category { get; set; }

        public virtual List<StrategyParameter> Parameters { get; set; }
    }
}
