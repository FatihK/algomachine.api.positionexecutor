using System;
using System.Collections.Generic;

namespace AlgoMachine.Core.Models
{
    public class Account
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string SecurityCode { get; set; }
        public string PublicKey { get; set; }
        public string SecretKey { get; set; }
        public DateTime AddedDate { get; set; }
        public DateTime ExpireDate { get; set; }

        public virtual List<Portfolio> Portfolios { get; set; }
    }
}