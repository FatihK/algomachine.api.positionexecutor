using Microsoft.AspNetCore.Mvc;
using AlgoMachine.Core.Api;

namespace AlgoMachine.Core.Api
{
    public abstract class AbstractController:ControllerBase
    {

        protected IActionResult ApiResponse<T>(T data, string message = null)
        {
            return this.StatusCode(200, new ApiResponse<T>(data, message));
        }
        protected IActionResult ApiErrorResponse(string message = null, int errorCode = 500)
        {
            return this.StatusCode(errorCode, new ApiResponse<string>(message));
        }
    }

}