using System.Threading.Tasks;

namespace AlgoMachine.Core.Interfaces
{
    public interface ITradeService
    {
        Task InitUserListenersAsync();
        Task ListenTickerUpdatesAsync();
        Task StopListenTickerUpdatesAsync();
    }
}