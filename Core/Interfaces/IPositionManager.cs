using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using AlgoMachine.Core.Models;
using CM = AlgoMachine.Core.Models;

namespace AlgoMachine.Core.Interfaces
{
    public interface IPositionManager{

        Task CreatePositionAsync(Position position);
        Task EndPositionAsync(Position position);
    }
}