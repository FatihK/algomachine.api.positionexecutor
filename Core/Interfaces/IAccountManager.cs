using System.Collections.Generic;
using System.Threading.Tasks;
using AlgoMachine.Core.Models;

namespace AlgoMachine.Core.Interfaces
{
    public interface IAccountManager
    {
        List<Account> GetActiveAccounts();
        Task<Account> GetSingleAccountAsync(int id);
        Task<Account> GetSingleAccountAsync(string securityCode);
        Task<Account> CreateAccountAsync(Account account);
        Task<Account> UpdateAccountAsync(Account account);
        Task<bool> DeleteAccountAsync(Account account);
    }
}