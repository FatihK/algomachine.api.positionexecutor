using AlgoMachine.Api.PositionExecutor;
using AlgoMachine.Core.Interfaces;
using AlgoMachine.Service.Managers;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using AlgoMachine.Data.Repositories;
using AlgoMachine.Data;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using AlgoMachine.Service;
using AlgoMachine.Service.PositionExecutor;
using Binance.Net;
using Microsoft.Extensions.Logging;
using System;
using Binance.Net.Clients;
using Binance.Net.Interfaces.Clients;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// Auto Mapper Configurations
var mapperConfig = new MapperConfiguration(mc =>
{
    mc.AddProfile(new MappingProfile());
});

System.AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);//For Postgresql datetime equels
IMapper mapper = mapperConfig.CreateMapper();
builder.Services.AddSingleton(mapper);

builder.Services.AddDbContext<DataContext>(options => 
    options.UseNpgsql("User ID=postgres;Password=1326;Host=79.143.187.71;Port=5432;Database=PositionExecutor.Dev;CommandTimeout=300;")//Configuration.GetConnectionString("PostgreConnection"))
    );
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHostedService<BinancePriceFetchHostedService>();
builder.Services.AddSingleton<IPositionManager, PositionManager>();
builder.Services.AddSingleton<IAccountManager, AccountManager>();
builder.Services.AddSingleton<IRepository, DefaultRepository>();
builder.Services.AddSingleton<ITradeService, TradeService>();

// builder.Services.AddBinance((restClientOptions, socketClientOptions) => {
//     restClientOptions.CoinFuturesApiOptions.BaseAddress="https://testnet.binance.vision";
//     restClientOptions.UsdFuturesApiOptions.BaseAddress="https://testnet.binance.vision";
//     restClientOptions.LogLevel = LogLevel.Trace;

//     socketClientOptions.UsdFuturesStreamsOptions.BaseAddress="wss://testnet.binance.vision/ws";
//     socketClientOptions.AutoReconnect=true;
//     socketClientOptions.ReconnectInterval = TimeSpan.FromSeconds(15);
//     socketClientOptions.LogLevel=LogLevel.Trace;
// });
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
app.Run();
